#!/usr/bin/python

import os
import shutil
import datetime

target = ["3icp","pflash1","rainbow5640","rainbow6440","rainbowbinary16242020","rainbowbinary256181212","tts6440"]

strdate = datetime.datetime.now().strftime("%Y%m%d")

from subprocess import call

for tar in target :
  call(["tar", "zcvf", tar + "-ref-"+ strdate + ".tar.gz" , "./crypto_sign/"+tar ])


